<?php
	namespace LayerC\lib\core;

	use LayerC\lib\core\LayerCLexer;



	class Reader
	{
		private $tags = [
			'VALUE'	 => ['{{', '}}'],
			'METHOD' => ['{%', '%}']
		];
		private $html, $cursor = 0, $lineo = 1, $length;
		public function __construct($html)
		{
			$this->html = preg_replace('/(\r\n|\r|\n)/', '\n', $html);

			$this->length = strlen($html);
		}
		public function addTag($name, $value)
		{
			$this->tags[$name] = $value;
		}
		public function make()
		{
			$this->Find();
		}	
		private function Find()
		{
			$tokens = $this->Generatetokens();
			/*preg_match_all(
				$tokens, 
				$this->html, 
				$m, 
				PREG_OFFSET_CAPTURE
			);
			$founds = $m[1];
			for($t=0; $t<count($founds); $t++)
			{
				if(isset($founds[$t][0]))
				{

				}
			}*/
			$this->next();
		}
		private function next()
		{
			preg_match('/(.*?)(\{[%\{#]|\$(?!\$))/A', $this->html, $match,
			        NULL, $this->cursor);
			$this->cursor += strlen($match[0]);
			$this->lineno += substr_count($match[0], '\n');
			if($this->cursor>=$this->length) return false;
			//$this->next();
			echo $this->cursor;

			print_r($match);
		}
		private function Generatetokens()
		{
			$regex = '/(';
			foreach($this->tags as $tag)
			{
				$regex.=preg_quote($tag[0], '/').'|'.preg_quote($tag[1], '/').'|';
			}
			return substr($regex, 0, strlen($regex)-1).')?/s';
		}

	}	
?>