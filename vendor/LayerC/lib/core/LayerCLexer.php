<?php
	namespace LayerC\lib\core;
	use LayerC\lib\core\LayerCToken;

	class LayerCLexer
	{
		private $html;
		private $cursor = 0;
		private $lineo = 1;
		private $position, $pushedBack;

		const DATA = 0;
		const BLOCK = 2;
		const TEXT = 3;
		const POS_VAR = 4;


		private $METHOD = 1;
		const REGEX_NAME = '/[A-Za-z_][A-Za-z0-9_]*/A';
		const REGEX_STRING = '/(?:"([^"\\\\]*(?:\\\\.[^"\\\\]*)*)"|\'([^\'\\\\]*(?:\\\\.[^\'\\\\]*)*)\')/Asm';


		public function __construct($code)
		{
			$this->html = preg_replace('/(\r\n|\r|\n)/', '\n', $code);
			$this->position = self::DATA;
			$this->pushedBack = [];
		}

		public function nextToken()
		{
			
			if(!empty($this->pushedBack))
				return array_shift($this->pushedBack);
			if($this->cursor>=strlen($this->html))
				return LayerCToken::EOF($this->lineo);


			switch ($this->position) 
			{
				case self::DATA:
					$tokens = $this->LexData(); break;
				case self::POS_VAR:
					$tokens = $this->LexVar(); break;	
				case self::BLOCK:
					$tokens = $this->LexBlock(); break;
			}

			if(!is_array($tokens))
				return $tokens;
			else if(empty($tokens))
				return $this->nextToken();
			else if(count($tokens)>1)
			{
				$first = array_shift($tokens);
				$this->pushedBack = $tokens;
				return $first;
			}
			return $tokens[0];
		}

		private function LexData()
		{
			$match = NULL;
			if(!preg_match('/(.*?)(\{[%\{#]|\$(?!\$))/A', $this->html, $match,
			        NULL, $this->cursor))
			{
				$rv = LayerCToken::Text(substr($this->html, $this->cursor), $this->lineo);
				$this->cursor = strlen($this->html);
				//throw new \Exception("END ");
				return $rv;
			}
			
			$this->cursor += strlen($match[0]);
			$lineo  = $this->lineo;
			$this->lineno += substr_count($match[0], '\n');
			$text = $match[1];
			///print_r($match);
			if(!empty($text))
			{
				$result = [LayerCtoken::Text($text, $lineo)];
				$lineo += substr_count($text, "\n");
			}
			else
				$result = [];
			
			
			if(($token = $match[2])!='$')
			{
				$result[] = new LayerCToken(LayerCToken::BLOCK_START, '', $lineo);				
				$this->position = self::BLOCK;
			}
			else if(isset($this->html[$this->cursor])
				&& $this->html[$this->cursor]=='{')
			{
				$this->cursor++;
				$result[] = new LayerCToken(LayerCToken::VARIABLE_START,
			                           '', $lineo);
				$this->position = self::POS_VAR;
				//print_r($result);
				
			}
			else if(preg_match(self::REGEX_NAME, $this->html, $match,
			            NULL, $this->cursor))
			{
				$result[] = new LayerCToken(LayerCToken::POS_VAR_START, '', $lineo);
				$result[] = LayerCToken::Name($match[0], $lineo);
				$this->cursor+=strlen($match[0]);
				while(isset($this->html[$this->cursor]) 
					&& $this->html[$this->cursor]==='.'){
					++$this->cursor;
					$result[] = LayerCToken::Operator('.', $this->lineo);
					if(preg_match(self::REGEX_NAME, $this->html, $match, NULL,
			       		$this->cursor))
					{
						$this->cursor += strlen($match[0]);
						$result[] = LayerCToken::Name($match[0],
								     $this->lineno);
					}else{
						--$this->cursor;
						break;
					}
				}
				$result[] = new LayerCToken(LayerCToken::POS_VAR_END,
						   '', $lineo);
			}
			return $result;
		}
		private function LexVar()
		{
			$match = NULL;
			
			if (preg_match('/([A-Za-z_][A-Za-z0-9_]*)\s*(\})/', $this->html, $match, NULL, $this->cursor)) 
			{

				$this->cursor += strlen($match[0]);
				$lineo = $this->lineno;
				$this->lineno += substr_count($match[0], '\n');
				$this->position = self::DATA;
				$return[] = LayerCToken::Name($match[1], $lineo);
				$return[] = new LayerCToken(LayerCToken::VARIABLE_END, '', $lineo);
				return $return;
			}
			return $this->lexExpression();
		}
		private function LexBlock()
		{
			$match = NULL;
			if (preg_match('/\s*%\}/A', $this->html, $match, NULL, $this->cursor)) 
			{
				
				$this->cursor += strlen($match[0]);
				$lineno = $this->lineno;
				$this->lineno += substr_count($match[0], '\n');
				$this->position = self::DATA;
				return new LayerCToken(LayerCToken::BLOCK_END, $match[0], $lineno);
			}
			return $this->lexExpression();
		}
		private function lexExpression()
		{

			///quitar espacios en blanco
			while (preg_match('/\s+/A', $this->html, $match, NULL,
			          $this->cursor)) {
				$this->cursor += strlen($match[0]);
				$this->lineno += substr_count($match[0], '\n');
			}
			if (preg_match(self::REGEX_NAME, $this->html, $match, NULL,
			       $this->cursor)) {
				
				$this->cursor += strlen($match[0]);
				return LayerCToken::Name($match[0], $this->lineno);
			}
			if (preg_match(self::REGEX_STRING, $this->html, $match, NULL,
			       $this->cursor)) {
				
				$this->cursor += strlen($match[0]);
				$this->lineno += substr_count($match[0], '\n');
				$value = stripcslashes(substr($match[0], 1, strlen($match[0]) - 2));
				return LayerCToken::String($match[0], $this->lineno);
			}
			throw new \Exception("CUAL CARÁCTER: ");
			//return false;
		}

	}
?>