<?php
	namespace LayerC\lib\core;

	class LayerCToken
	{
		public $value, $type, $lineo, $tokens;	
		const DATA_TYPE = 1;
		const VARIABLE_START = 2;
		const DATA_START = 3;
		const VARIABLE_END = 4;
		const DATA_END = 5;
		const TEXT = 0;
		const NAME = 6;
		const BLOCK_START = 7;
		const BLOCK_END = 8;
		const EOF_TYPE = -1;
		const STRING_TYPE = 11;
		const OPERATOR = 9;
		const VAR_NAME = 12;


		public function __construct($type, $value, $lineo)
		{
			$this->value = $value;
			$this->type = $type;
			$this->lineo = $lineo;
		}
		///busco que sea del mismo tipo
		public function test($type, $second)
		{
			return ($this->type==$type || $this->type==$second);
		}
		public static function EOF($lineo)
		{
			return new LayerCToken(self::EOF_TYPE, '',  $lineo);
		}
		public static function Variable($value, $lineo)
		{
			return new LayerCToken($value,  $lineo);
		}
		public static function String($value, $lineo)
		{
			return new LayerCToken(self::STRING_TYPE, $value,  $lineo);
		}
		public static function Data( $value, $lineo)
		{
			return new LayerCToken(self::DATA_TYPE, $value, $lineo);
		}
		public static function Text($value, $lineo)
		{
			return new LayerCToken(self::TEXT, $value,  $lineo);
		}
		public static function Name($value, $lineo)
		{
			return new LayerCToken(self::NAME, $value,  $lineo);
		}
		public static function Operator($value, $lineo)
		{
			return new LayerCToken(self::OPERATOR, $value,  $lineo);
		}
		public static function VarName($value, $lineo)
		{
			return new LayerCToken(self::VAR_NAME, $value, $lineo);
		}
	}
?>