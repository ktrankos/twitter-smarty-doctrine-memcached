<?php
	namespace LayerC\methods;
	interface IMethod
	{
		public function get();
		public function Execute($code, $load, $tags);
	}
?>