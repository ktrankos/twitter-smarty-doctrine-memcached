<?php
	namespace LayerC\methods;

	use LayerC\methods\ILayerC;
	use LayerC\lib\LayerCFunctions;
	class RawCode implements ILayerC
	{
		private $code, $found;
		public function __construct()
		{
			
			LayerCFunctions::addFunction(
			['raw'=>[
	            "class"  => $this,
	            "method" => '_Raw',
	            "pattern"=> '/\{\{(.*)\|\s*raw\}\}/'
        	]]
			);
		}
		public function get()
		{
			foreach($this->found as $key=>$val)
			{
				$pattern[] = '/\b'.$key.'\b/';
				$replacement[] = $val; 
			}
			
			$this->code = preg_replace($pattern, $replacement, $this->code);
					
			return htmlspecialchars($this->code);
		}
		public function _Raw($code, $found)
		{
			$this->code = $code;
			$this->found = $found;
			return $this->get();
			
		}
	}
?>