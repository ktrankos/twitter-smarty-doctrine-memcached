<?php
namespace Ats;

/**
 * Description of MePhp
 *
 * @author watson
 * @date   17-sep-2014
 */

use Ats\Router\Route;
use Ats\Router\Router;

class MePhp
{
    

    public function run()
    {
        
 
        return  Router::init()->get(
            'index', new Route('/home', array(
                '_controller'=>'Ton:Controller:SiteController:Camaron',                
                '_defaults'=>array('id' =>1, 'name'=>" OLO", "casa"=>"la del pueblo", 'tomo'=>'golo'),
                '_inject' => array('Necora'=>'class:Ton\data\Necora'),
                '_method'=>array('Post')               
            ))
        )->add(
            'demo', new Route('/casa/sitar', array(
                '_controller'=>'Ton:Controller:SiteController:Demo',
                '_method'=>array('Post', 'get'),
                '_defaults'=>array('id'=>1, 'name'=>'alvaro')
            ))
        )->add(
            'home', new Route('/ho', array(
                '_controller'=>'Ton:Controller:SiteController:Alone',
                '_method'=>array('Post', 'get'),
                '_defaults'=>array('name'=>'alvaro')
            ))
        )->add(
            'tres', new Route('/home/sanson/colega', array(
                '_controller'=>'Ton:Controller:SiteController:Loco',
                '_method'=>array('Post', 'get')
            ))
        );
    }
}
?>
