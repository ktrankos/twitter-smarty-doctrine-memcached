<?php
	namespace Ats\Router;

	trait MatchRoute
	{
		public function Match($uri)
		{
			
			$uri = $this->clean($uri);
			$this->uri = $this->clean($this->uri);
			$routeURIparts = $this->regExp->getPartsURI($this->uri);
			$routeURIbase = $this->regExp->getPartsURI($this->uri);
			$URIparts = $this->regExp->getPartsURI($uri);
			//opcionales obligatorios
			$routeURImusts = $this->regExp->getOptional($this->uri);
			$this->allParams($routeURIparts, $routeURImusts[0], $this->defaults);
			//aqui empieza el algoritmo
			///
			if(count($URIparts)>count($routeURIparts))
				return 0;
			
			if((count($routeURIbase)-count($routeURImusts[0])) > count($URIparts))
				return 0;

			$points = 0;

			

			if(count($URIparts)==count($routeURIparts))
				$points++;
			

			
			//y entonces cuando ya tengo los datos necesarios y limpios
			// combino posibles y obligatorios
			for($t=0; $t<count($URIparts); $t++)
			{
				if(!isset($routeURIparts[$t]))
					return 0;

				if(!preg_match_all('/^\{(.*)\}$/', $routeURIparts[$t], $m))
				{
					if($routeURIparts[$t]==$URIparts[$t])
						$points++;
					else
						return 0;

				}else
				{
					$points+=0.5;
					$this->defaults[$m[1][0]] = $URIparts[$t];
				}
			}

			return $points;
		}
	}
?>