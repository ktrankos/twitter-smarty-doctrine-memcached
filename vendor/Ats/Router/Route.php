<?php
	namespace Ats\Router;

	use Ats\Router\Regxer;
	
	class Route
	{
		use BaseRoute;
		use MatchRoute;

		private $defaults=array();
		private $uri;
		
		private $controller;
		private $regExp;
		private $before = FALSE, $after = FALSE, $inject=FALSE;

		public function __construct($uri, $params)
		{
			$this->regExp = new Regxer($uri);
			////////////////////////////////
			if(isset($params['method'])) $this->setMethod($params['method']);
			else $this->setMethod('get');
			///////////////////////////////////////////////////////////
			if(isset($params['defaults'])) $this->defaults = $params['defaults'];
			if(isset($params['before'])) $this->before = $params['before'];
			if(isset($params['after'])) $this->after = $params['after'];
			if(isset($params['inject'])) $this->inject = $params['inject'];

			$this->uri = $uri;
			$this->controller = $params['controller'];
		}

		
		public function Before()
		{
			if($this->before)
				call_user_func($this->before);			
		}
		public function After()
		{
			if($this->after)
				call_user_func($this->after);			
		}
		public function test(){
			echo "MAURO";
		}

		
		//////////////////////////////7
		private function clean($uri)
		{
			if(substr($uri, -1)=='/')
				return substr($uri, 0, strlen($uri)-1);
			return $uri;
		}
		private function allParams(&$URI, $musts, $optionals)
		{

			//// solo si optativos mayores que los obligatorios
			if(count($optionals) > count($musts))
			{
				$c = 0;
				foreach ($optionals as $key => $value) {
					if(!isset($musts[$c]))
						$URI[] = '{'.$key.'}';
					$c++;
				}
			}
		}

	}
?>