<?php
	namespace Ats\Event;
	final class Events
	{
		const basic = "basic.ton";
		const onBeforeController = "before.on.controller.loaded";
		const onAfterController  = "after.on.controller.loaded";
	}
?>