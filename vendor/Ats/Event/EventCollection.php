<?php
	namespace Ats\Event;

	use Ats\Event\Event;
	class EventCollection
	{
		private $events;
		public function __construct()
		{
			$this->events = array();
		} 
		public function Find($value)
		{
			if(!isset($this->events[$value])) return false;
			foreach($this->events[$value] as $e)			
				$e[0]->$e[1]();
			
		}
		public function Append(Event $e, $callback)
		{
			if(isset($this->events[$e->getAssoc()]))
			{
				$this->events[$e->getAssoc()][$e->getName()] = [$e, $callback];
				return true;
			}
			$this->events[$e->getAssoc()] = [$e->getName() => [$e, $callback]];
			return true;
		}
	}
?>