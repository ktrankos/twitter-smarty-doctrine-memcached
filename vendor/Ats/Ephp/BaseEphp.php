<?php
	namespace Ats\Ephp;
	trait BaseEphp
	{
		private $eventCollection, $orm, $tpl, $bundles, $routes
		, $route, $config, $configFolder="/../../../app/config/";

		public function setORM($orm=NULL)
		{
			if($orm==NULL)
			{
				new ORM\DoctrineORM($orm, $this->config);
			}
			$this->orm = $orm;
		}
		public function getORM()
		{
			return $this->orm;
		}
		public function setTPL($tpl=NULL)
		{
			if($tpl==NULL)
			{
				if($this->getTplEngine()=="smarty")
				{
					for($s=0; $s<count($this->bundles); $s++)
					{
						$bundle = preg_split('/\\\/', get_class($this->bundles[$s]));
						$dirs[$bundle[0]] = __DIR__.'/../../../'.$bundle[0].'/view';
					}
					new TPL\SmartyTPL($tpl, $dirs, $this->memcache);
				}
				if($this->getTplEngine()=="LayerC")
				{
					
					new \LayerC\LayerC($tpl, $this->route, $this->config['LayerC']['cache']);
				}
				
			}			
			$this->tpl = $tpl;
		}
		public function getTPL()
		{
			return $this->tpl;
		}
		public function AddEvent($event, $callback)
		{
			$this->eventCollection->Append($event, $callback);
		}
		//////////////////
		private function getTplEngine()
		{
			if(isset($this->config['smarty']) && $this->config['smarty']) return 'smarty';
			if(isset($this->config['LayerC']) && $this->config['LayerC']) return 'LayerC';
			return false;
		}
	}
?>