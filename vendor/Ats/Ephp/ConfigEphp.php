<?php
	namespace Ats\Ephp;
	use Yaml\Yaml;
	use Ats\Router\Route;
	use Ats\Router\Router;

	trait configEphp
	{
		private $budlesConfig, $Router, 
		$basedir = '/../../../src/';

		public function config()
		{	
			for($s=0; $s<count($this->bundles); $s++)
				$this->setRouting($this->bundles[$s]);
		}
		
		private function setRouting($bundle)
		{
			$this->Router = new Router();
			$bundle = preg_split('/\\\/', get_class($bundle));
			$routingYML = __DIR__.$this->basedir.$bundle[0].'/config/routing.yml';
			if(file_exists($routingYML))
			{				
				$routes = Yaml::parse(file_get_contents($routingYML));
				foreach($routes as $key=>$value)
				{
					$this->routeCollection[$bundle[0]][]= $this->Router->add($value['path'], $value);
				}
			}

		}

	}
?>