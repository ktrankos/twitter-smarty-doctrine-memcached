<?php
	namespace Ats\Ephp;

	use Ats\Event\Events;

	trait RunEphp
	{
		
		public function run()
		{

			
			//$route = $router->Find();
			
			$base = preg_split('/\:/',  $this->route->getController());
			$bundle = $base[0].'\\'.$base[0].'Bundle';
			//init the bundle, para poder inyectar compponenetes genericos, claaaaaro
			//$bundle = new $bundle($this);
			
			
			$onBefore = $this->eventCollection->Find(Events::onBeforeController);

			$method = $base[count($base)-1];
			unset($base[count($base)-1]);
			$sClass = implode('\\', $base);
			$this->route->Before();

			$class = new $sClass($this->orm, $this->getTPL());
			call_user_func_array(array($class, $method), $this->route->getDefaults());
			$this->route->After();
		}
	}
?>