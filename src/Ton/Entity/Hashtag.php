<?php
namespace Ton\Entity;

use Ton\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Ton\Entity\Hashtag
 *
 * @ORM\Table(name="hashtag")
 * @ORM\Entity
 */
class Hashtag extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hashtag", type="string", length=255, nullable=false)
     */
    private $hashtag;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_id", type="string", length=110, nullable=false)
     */
    private $lastId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hashtag
     *
     * @param string $hashtag
     * @return Hashtag
     */
    public function setHashtag($hashtag)
    {
        $this->hashtag = $hashtag;

        return $this;
    }

    /**
     * Get hashtag
     *
     * @return string 
     */
    public function getHashtag()
    {
        return $this->hashtag;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Hashtag
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set lastId
     *
     * @param integer $lastId
     * @return Hashtag
     */
    public function setLastId($lastId)
    {
        $this->lastId = $lastId;

        return $this;
    }

    /**
     * Get lastId
     *
     * @return integer 
     */
    public function getLastId()
    {
        return $this->lastId;
    }
    public function __construct()
    {
        $this->setCreatedAt(new \Datetime());
        $this->tweet = new ArrayCollection();
    }
    /**
     * @ORM\OneToMany(targetEntity="Ton\Entity\Tweet", mappedBy="hashtag")
     **/
    private $tweets;
    
    /**
     * Set tweet
     *
     * @param Tweet $tweet
     * @return Hashtag
     */
    public function setTweet(Tweet $tweet = null)
    {
        $this->tweet = $tweet;

        return $this;
    }

    /**
     * Get tweet
     *
     * @return Hashtag 
     */
    public function getTweet()
    {
        return $this->tweet;
    }
}
