{extends file="layout.tpl"}
{block name=title}Hola a todo el mundo{/block}
{block name=body}
	<form action="/" method="post">
		<input type="text" name="hashtag" placeholder="introduce tu hashtag"/> 
		<input type="submit" value="Obtenlos" />
	</form>	
	{if $error!=false}
		<p style="color:#ff0000;">{$error}{if $hashtag ne ""} #{$hashtag}{/if}</p>
	{else}
		{if $hashtag ne ""}
		<p>Resultados obtenidos por el hashtag: #{$hashtag}</p>		
		{/if}
	{/if}
	<table cellspacing="0" cellpading="0" border="1"> 
	{foreach $listado as $hashtag}
		<thead>
		<tr>
	    	<td colspan="2" style="background:#999;">#{$hashtag.hashtag}<span style="color:#fff">[{$hashtag.tweets|@count}]</span>  ||||| {$hashtag.createdAt|date_format:"%d-%m-%Y %H:%M:%S"}</td>	    			
		</tr>
		</thead>
		{foreach $hashtag.tweets as $tweet}
		<tbody>
		<tr>
			{assign "arrtweet" $tweet.tweet|unserialize nocache}	
			<td>
				<a href="{$arrtweet.user->url}" target="_blank">
					<img src="{$arrtweet.user->profile_image_url}" alt="{$arrtweet.user->name}"/>
				</a>
			</td>
	    	
	    	<td>{twitter_text text=$arrtweet.text hashtag=$hashtag.hashtag }
	    	{foreach $arrtweet.entities->urls as $url}
	    	<br /><a href="{$url->url}" target="_blank">{$url->display_url}</a>
	    	{/foreach}
	    	<p>
	    		<span style="color:#999">Retweets: {$arrtweet.retweet_count}</span>
	    		<span style="color:#999">Favoritos: {$arrtweet.favorite_count}</span>
	    	</p>
	    	<p>{$arrtweet.created_at|twitter_date}</p>
	    	</td> 			
		</tr>	
		</tbody>
		{/foreach}
	{/foreach}	
	</table>	
{/block}