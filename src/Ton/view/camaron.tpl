{%extends 'Ton:layout.tpl' %}
{%piece 'title'%}con nombre ${name}{%endpiece%}
{%piece 'css'%}
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css" media="screen">
{%endpiece%}
{%piece 'js'%}
<script
  src="/components/webcomponentsjs/webcomponents.js">
</script>

<link rel="import"
  href="/components/font-roboto/roboto.html">
<link rel="import"
  href="/components/core-header-panel/core-header-panel.html">
<link rel="import"
  href="/components/core-toolbar/core-toolbar.html">
<link rel="import"
  href="/components/paper-tabs/paper-tabs.html">
<link rel="import"
  href="/ats/ats-card.html">  
<style>
html,body {
  height: 100%;
  margin: 0;
  background-color: #E5E5E5;
  font-family: 'RobotoDraft', sans-serif;
}
core-header-panel {
  height: 100%;
  overflow: auto;
  -webkit-overflow-scrolling: touch;
}
core-toolbar {
  background: #03a9f4;
  color: white;
}
#tabs {
  width: 100%;
  margin: 0;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  text-transform: uppercase;
}
</style>
{%endpiece%}
{% piece 'body'%}
<core-header-panel>

  <core-toolbar>

  <paper-tabs id="tabs" selected="all" self-end>
    <paper-tab name="all">All</paper-tab>
    <paper-tab name="favorites">Favorites</paper-tab>
    <paper-tab name="chicote">Chicote</paper-tab>
  </paper-tabs>

  </core-toolbar>
  <div class="container" layout vertical center>

	  <ats-card name="barajas" items='[{"name":"oswaldo"}]'>
	    <img width="70" height="70"
	      src="../images/avatar-07.svg">
	    <h2>Another Developer</h2>
	    <p>I'm composing with shadow DOM!</p>
	  </ats-card>

	</div>

</core-header-panel>

{%endpiece%}