<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<title>{%piece 'title'%} Default Page Title{%endpiece%}</title>
  	<meta name="viewport"
    content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes">
  	{%piece 'css'%}{%endpiece%}
  	{%piece 'js'%}{%endpiece%}
</head>
<body fullbleed vertical layout unresolved>
	{%piece 'body'%}{%endpiece%}
</body>
</html>