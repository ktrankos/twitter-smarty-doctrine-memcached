<?php
namespace Ton\Console;
use Ton\Services\TweetService;
use Yaml\Yaml;

class Application
{
	public function run()
	{
		$config = Yaml::parse(file_get_contents(__DIR__.'/../../../app/config/config.yml'));
		if(isset($_SERVER['argv']) && count($_SERVER['argv'])>1)
		{
			$post = $_SERVER['argv'][1];
			if(preg_match_all('/ĥashtag (\-\-h)|(\-\-help)$/', $post, $m)){
				echo "Ayuda:\n- Formato: hashtag:valor\n- El valor sin espacio ni almohadilla\n";
				return false;
			}
			if(preg_match_all('/^(hashtag)\:([a-zäáàëéèíìöóòúùñçA-ZÁÉÍÓÚÀÈÌÒÙäëïöüÑ0-9_]*)$/', $post, $m)){
				Tweetservice::addHashTag($m[2][0]);			
				echo "Visita la web para ver los resultados.\n";
			}

			
			if(preg_match_all('/^clear\:cache\:(.*)$/', $post, $m))
			{
				$c = $config['_memcache'];
				$memcached = new \Memcached();
    			$memcached->addServer($c['server'], $c['port'], $c['weight']);
				if($memcached->get($m[1][0])) {
					
					$memcached->delete($m[1][0]);
					echo "cache cleared : '".$m[1][0]."'\n";
				}else{
					echo "no cache creado '".$m[1][0]."'\n";
				}
			}
			if(preg_match_all('/^get\:cache\:(.*)$/', $post, $m))
			{
				if(isset($config['_memcache']) && $config['_memcache'])
				{
					$c = $config['_memcache'];
					$memcached = new \Memcached();
    				$memcached->addServer($c['server'], $c['port'], $c['weight']);
    				if($memcached->get($m[1][0]))
    				{
    					print_r($memcached->get($m[1][0]));
    					echo "\n";
    				}else{
    					echo "---------------NO EXISTE '".$m[1][0]."'   -----------------\n";
    				}
				}
				
			}
			
			
		}
	}
}
?>