<?php
	function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$time_start = microtime_float();

    require_once __DIR__.'/../app/config/TonKernel.php';
    use Ats\Ephp\AtsEphp;
    $ephp = new AtsEphp($bundles);
    $ephp->start();
    $time_end = microtime_float();
    $time = $time_end - $time_start;

echo "No se hizo nada en $time segundos\n";
?>